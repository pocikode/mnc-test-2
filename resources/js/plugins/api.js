import axios from 'axios'
import Vue from 'vue'

axios.defaults.baseURL = document.head.querySelector('meta[name="api-base-url"]').content
axios.defaults.headers.common['Accept'] = 'application/json'

const toastConfig = {
  position: "bottom-left",
  timeout: 3000,
  closeOnClick: true,
  pauseOnHover: false,
  icon: true,
  rtl: false
}

// request handling
axios.interceptors.request.use(
  (request) => {
    request.toast = {
      show: false,
      type: 'success',
      ...(request.toast || {})
    }

    return request
  }
)

// response handling
axios.interceptors.response.use(
  (response) => {
    // Response success
    const request = response.config
    let message = response.data.message || 'Success'

    if (request.toast.show) {
      if (request.toast.type) {
        Vue.$toast[request.toast.type](message, toastConfig)
      } else {
        Vue.$toast.success(message, toastConfig)
      }
    }

    return response
  },
  (error) => {
    // Response error
    let message = error.response.data.message || 'Internal Server Error'

    Vue.$toast.error(message, toastConfig)

    return Promise.reject(error)
  }
)

export default axios