const rupiah = (number) => {
  let result = ''

  let numberRev = number.toString().split('').reverse().join('')

  for (let i = 0; i < numberRev.length; i++) {
    if (i%3 == 0) {
      result += numberRev.substr(i, 3) + '.'
    }
  }

  return 'Rp' + result.split('', result.length - 1).reverse().join('')
}

export {
  rupiah
}