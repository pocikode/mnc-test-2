import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const state = {
  categories: [],
  sizes: [],
  colors: [],
  products: {
    page: 1,
    lastPage: 1,
    loading: true,
    data: [],
    links: {}
  },
  product: {}
}

const getters = {
  products(state) {
    return state.products
  },
  categories(state) {
    return state.categories
  },
  sizes(state) {
    return state.sizes
  },
  colors(state) {
    return state.colors
  }
}

const mutations = {
  setCategories(state, data) {
    state.categories = data
  },
  setSizes(state, data) {
    state.sizes = data
  },
  setColors(state, data) {
    state.colors = data
  },
  setProducts(state, data) {
    let no = data.meta.from
    let result = data.result

    result.forEach(product => {
      product.no = no
      no++
    })

    state.products.data = result
    state.products.page = data.meta.current_page
    state.products.lastPage = data.meta.last_page
    state.products.links = data.links
  },
  setProductsLoading(state, loading) {
    state.products.loading = loading
  }
}

const actions = {
  fetchProducts({ commit }, options = {}) {
    let page = options.hasOwnProperty('page') ? options.page : 1
    let url = `products?page=${page}`

    if (options.hasOwnProperty('search')) {
      url += `&search=${options.search}`
    }

    if (options.hasOwnProperty('category_id')) {
      url += `&category_id=${options.category_id}`
    }

    axios.get(url)
      .then(res => {
        commit('setProducts', res.data)
      })
  },
  deleteProduct({ dispatch }, id) {
    axios.delete(`products/${id}`, { toast: { show: true } })
      .then(res => {
        dispatch('fetchProducts')
      })
  },
  storeProduct({ dispatch }, data) {
    return axios.post('products', data, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      toast: { show: true }
    })
      .then(res => {
        dispatch('fetchProducts')
        return true
      })
      .catch(err => {
        return false
      })
  },
  fetchCategories({ commit }) {
    axios.get('categories')
      .then(res => {
        commit('setCategories', res.data.result)
      })
  },
  fetchSizes({ commit }) {
    axios.get('sizes')
      .then(res => {
        commit('setSizes', res.data.result)
      })
  },
  fetchColors({ commit }) {
    axios.get('colors')
      .then(res => {
        commit('setColors', res.data.result)
      })
  }
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})