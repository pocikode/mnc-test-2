<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::query()->insert([
            ['name' => 'Merah', 'hex' => 'ff0000'],
            ['name' => 'Biru', 'hex' => '0000ff'],
            ['name' => 'Hitam', 'hex' => '000000'],
            ['name' => 'Abu-abu', 'hex' => '808080'],
        ]);
    }
}
