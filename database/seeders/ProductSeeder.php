<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::where('name', 'Pakaian')->first();

        $product = Product::create([
            'category_id' => $category->id,
            'name' => 'Baju Kerja SPIN',
            'description' => 'Pakaian yang digunakan untuk bekerja di SPIN',
            'images' => [
                'https://ladangkreasikonveksi.files.wordpress.com/2018/06/mnc-vision-pendek-02.jpg',
                'https://ladangkreasikonveksi.files.wordpress.com/2018/09/kemeja-mnc-baru-01.jpg?w=525&h=525',
                'https://cf.shopee.co.id/file/dde945704af58d32f08b547e2f6e7856'
            ],
        ]);

        $product->entries()->create([
            'size_id' => 2,
            'color_id' => 2,
            'qty' => 12,
            'price' => 150000,
        ]);

        $product->entries()->create([
            'size_id' => 3,
            'color_id' => 2,
            'qty' => 7,
            'price' => 155000,
        ]);

        $product->entries()->create([
            'size_id' => 4,
            'color_id' => 2,
            'qty' => 14,
            'price' => 160000,
        ]);

        $product->entries()->create([
            'size_id' => 2,
            'color_id' => 3,
            'qty' => 14,
            'price' => 150000,
        ]);

        $product->entries()->create([
            'size_id' => 3,
            'color_id' => 3,
            'qty' => 4,
            'price' => 155000,
        ]);

        Product::factory()
            ->count(25)
            ->hasEntries(random_int(10, 20))
            ->create();
    }
}
