<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::query()->insert([
            ['name' => 'Pakaian'],
            ['name' => 'Celana'],
            ['name' => 'Topi'],
            ['name' => 'Jas'],
            ['name' => 'Sepatu'],
            ['name' => 'Jaket'],
            ['name' => 'Tas'],
            ['name' => 'Dompet'],
        ]);
    }
}
