<?php

namespace Database\Factories;

use App\Models\Color;
use App\Models\ProductEntry;
use App\Models\Size;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductEntryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductEntry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'size_id' => $this->getRandomSize(),
            'color_id' => $this->getRandomColor(),
            'qty' => $this->faker->randomDigit(),
            'price' => $this->faker->numberBetween(50000, 500000),
        ];
    }

    protected function getRandomSize()
    {
        $sizes = Size::all()->pluck('id');

        return$sizes->random();
    }

    protected function getRandomColor()
    {
        $colors = Color::all()->pluck('id');

        return $colors->random();
    }
}
