<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductListRequest;
use App\Http\Requests\ProductStoreRequest;
use App\Models\Product;
use App\Models\ProductEntry;
use Illuminate\Http\UploadedFile;

class ProductController extends Controller
{
    public function index(ProductListRequest $request)
    {
        $query = Product::with('category');

        if ($request->category_id) {
            $query = $query->where('category_id', $request->category_id);
        }

        $likeOperator = config('database.default') === 'pgsql'
            ? 'ilike'
            : 'like';

        if ($request->search) {
            $query = $query->where('name', $likeOperator, "%{$request->search}%");
        }

        $data = $query->paginate($request->limit ?? 10)
            ->appends($request->query());

        return response()->api($data);
    }

    public function show(Product $product)
    {
        $product->load('category', 'entries', 'entries.size', 'entries.color');

        return response()->api($product);
    }

    public function store(ProductStoreRequest $request)
    {
        try {
            \DB::beginTransaction();

            $product = Product::create([
                'category_id' => $request->category_id,
                'name' => $request->name,
                'description' => $request->description,
                'images' => $this->uploadImages($request->images),
            ]);

            $entries = is_array($request->entries)
                ? $request->entries
                : json_decode($request->entries, true);

            $this->insertEntries($product->id, $entries);

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
        }

        return response()->api($product);
    }

    public function destroy(Product $product)
    {
        foreach ($product->local_images as $image) {
            if (\Storage::exists("public/{$image}")) {
                \Storage::delete("public/{$image}");
            }
        }

        $product->delete();

        return response()->api();
    }

    protected function uploadImages(array $images)
    {
        $imagePath = [];

        /** @var UploadedFile $image */
        foreach ($images as $image) {
            $filename = \Str::uuid() . '.' . $image->extension();

            \Storage::putFileAs('public/images', $image, $filename);

            $imagePath[] = "images/{$filename}";
        }

        return $imagePath;
    }

    protected function insertEntries(int $productId, array $entries)
    {
        foreach ($entries as &$entry) {
            $entry['product_id'] = $productId;
        }

        ProductEntry::query()->insert($entries);
    }
}
