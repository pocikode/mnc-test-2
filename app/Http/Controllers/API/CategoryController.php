<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::query()
            ->select('id', 'name')
            ->get();

        return response()->api($data);
    }
}
