<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index()
    {
        $data = Color::query()
            ->select('id', 'name', 'hex')
            ->get();

        return response()->api($data);
    }
}
