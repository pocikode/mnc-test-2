<?php

namespace App\Http\Requests;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => ['required', 'exists:categories,id'],
            'name'          => ['required', 'string', 'max:255'],
            'description'   => ['required', 'string'],
            'images'        => ['required', 'array'],
            'images.*'      => ['required', 'image'],
            'entries'       => ['required'],
        ];
    }
}
