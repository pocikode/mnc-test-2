<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function entries()
    {
        return $this->hasMany(ProductEntry::class);
    }

    protected function getLocalImagesAttribute()
    {
        return json_decode($this->attributes['images'], true);
    }

    public function getImagesAttribute($values)
    {
        $images = json_decode($values, true);

        foreach ($images as &$image) {
            if (!\Str::startsWith($image, 'http')) {
                $image = url(\Storage::url($image));
            }
        }

        return $images;
    }

    public function setImagesAttribute($values)
    {
        $this->attributes['images'] = json_encode($values);
    }
}
